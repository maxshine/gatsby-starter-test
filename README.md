<p align="center">
	<a href="https://www.gatsbyjs.org">
		<img alt="Gatsby" src="https://lh5.googleusercontent.com/p/AF1QipNZwm-8h-dffKUqUYY8HSNguaVsVJj3mJvL4vK7=w160-h160-k-no" width="60" />
	</a>
</p>
<h1 align="center">Batch Development @ Christchurch</h1>

Kick off your project with this default boilerplate. This barebones starter ships with the main Gatsby configuration files you might need. 

_Have another more specific idea? You may want to check out our vibrant collection of [official and community-created starters](https://www.gatsbyjs.org/docs/gatsby-starters/)._

## 🚀 Quick start

1.  **Download.**

		Clone the repository

		```sh
		# downloading the starter
		git clone git:... <project-name>
		```

2.  **Install dependency.**

		Use the Gatsby CLI to create a new site, specifying the default starter.

		```sh
		# go into the project folder
		cd <project-name>
		# using npm to install all dependencies
		npm i
		```

3.  **Start developing.**

		Navigate into your new site’s directory and start it up.

		```sh
		gatsby develop
		```
		
## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

		.
		├── src
		├		├── components
		├		├── images
		├		├── pages
		├		├── styles
		├		└── templates
		├── .gitignore
		├── .prettierrc
		├── gatsby-browser.js
		├── gatsby-config.js
		├── gatsby-node.js
		├── gatsby-ssr.js
		├── LICENSE
		├── package-lock.json
		├── package.json
		├── postcss.config.js
		└── tailwind.config.lock

	1.  **`/node_modules`**: The directory where all of the modules of code that your project depends on (npm packages) are automatically installed.  
	
	2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser), like your site header, or a page template. “Src” is a convention for “source code”.
	
	3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.
	
	4.  **`.prettierrc`**: This is a configuration file for a tool called [Prettier](https://prettier.io/), which is a tool to help keep the formatting of your code consistent.
	
	5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.
	
	6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).
	
	7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.
	
	8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.
	
	9.  **`LICENSE`**: Gatsby is licensed under the MIT license.
	
	10.  **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. (You won’t change this file directly).
	
	11.  **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.
	
	12.  **`README.md`**: A text file containing useful reference information about your project.
	
	13.  **`yarn.lock`**: [Yarn](https://yarnpkg.com/) is a package manager alternative to npm. You can use either yarn or npm, though all of the Gatsby docs reference npm.  This file serves essentially the same purpose as `package-lock.json`, just for a different package management system.

## 🎓 Netlify

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

-   **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

-   **To dive straight into code samples head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the “Guides”, API reference, and “Advanced Tutorials” sections in the sidebar.

## 💨 Tailwind
The following plugins to make Tailwind work

- **tailwind** Tailwind library
- **gatsby-plugin-postcss** Gatsby plugin to handle PostCSS. It will listen every imported css file at JS.
- **purgecss-webpack-plugin** a Webpack plugin to remove unused css.
- **autoprefixer** a Postcss plugin

## 📡 netlify
- **gatsby-plugin-netlify** add support netlify _header, the setup mainly to support netlify service workers and createRedirect.actions [read more](https://www.netlify.com/blog/2018/06/28/5-pro-tips-and-plugins-for-optimizing-your-gatsby---netlify-site/).
- **gatsby-plugin-netlify-functions**


## 💫 Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)
