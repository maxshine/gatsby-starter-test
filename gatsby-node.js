const _ = require('lodash')
const Promise = require('bluebird')
const PurgeCssPlugin = require('purgecss-webpack-plugin')
const path = require('path')
const glob = require('glob')
const { createFilePath } = require('gatsby-source-filesystem')

const PATHS = {
	src: path.join(__dirname, 'src')
}

const purgeCssConfig = {
	paths: glob.sync(`${PATHS.src}/**/*.js`, { nodir: true }),
	extractors: [
		{
			extractor: class {
				static extract(content) {
					return content.match(/[A-Za-z0-9-_:/]+/g) || []
				}
			},
			extensions: ['js']
		}
	],
	// Adjust for each project
	whitelist: ['class-to-whitelist'],
	// Adjust for each project
	whitelistPatterns: [/body/, /headroom/, /ReactModal/, /ril/],
}

exports.createPages = ({ graphql, actions }) => {
	const { createPage } = actions
	return true
}

exports.onCreateWebpackConfig = ({ actions, stage }) => {
  if (stage.includes('develop')) {
		actions.setWebpackConfig({
			devtool: 'cheap-module-source-map',
		})
		return true
	}

  // Add PurgeCSS in production
  // See: https://github.com/gatsbyjs/gatsby/issues/5778#issuecomment-402481270
  if (stage.includes('build')) {
    actions.setWebpackConfig({
      plugins: [new PurgeCssPlugin(purgeCssConfig)],
    })
  }
}
/* END */
